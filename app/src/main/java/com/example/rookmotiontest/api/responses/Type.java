package com.example.rookmotiontest.api.responses;

public class Type {

    private SimpleType type;

    public SimpleType getType() {
        return type;
    }

    public void setType(SimpleType type) {
        this.type = type;
    }
}
