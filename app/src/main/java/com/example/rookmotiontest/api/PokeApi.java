package com.example.rookmotiontest.api;

import com.example.rookmotiontest.api.responses.PokemonResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PokeApi {

    @GET("pokemon/{id}")
    Observable<PokemonResponse> getRandomPokemon(@Path("id") long id);
}
