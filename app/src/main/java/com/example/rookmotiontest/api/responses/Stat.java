package com.example.rookmotiontest.api.responses;

import com.google.gson.annotations.SerializedName;

public class Stat {

    @SerializedName("base_stat")
    private long baseStat;
    private StatFeature stat;

    public long getBaseStat() {
        return baseStat;
    }

    public void setBaseStat(long baseStat) {
        this.baseStat = baseStat;
    }

    public StatFeature getStat() {
        return stat;
    }

    public void setStat(StatFeature stat) {
        this.stat = stat;
    }
}
