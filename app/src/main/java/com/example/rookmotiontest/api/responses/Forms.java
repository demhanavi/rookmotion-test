package com.example.rookmotiontest.api.responses;

import com.google.gson.annotations.SerializedName;

public class Forms {

    @SerializedName("front_default")
    private String defaultForm;

    public String getDefaultForm() {
        return defaultForm;
    }

    public void setDefaultForm(String defaultForm) {
        this.defaultForm = defaultForm;
    }
}
