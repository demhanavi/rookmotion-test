package com.example.rookmotiontest.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PokemonResponse {

    long id;
    String name;
    @SerializedName("sprites")
    Forms forms;
    List<Stat> stats;
    List<Type> types;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Forms getForms() {
        return forms;
    }

    public void setForms(Forms forms) {
        this.forms = forms;
    }

    public List<Stat> getStats() {
        return stats;
    }

    public void setStats(List<Stat> stats) {
        this.stats = stats;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }
}
