package com.example.rookmotiontest.api.responses;

public class StatFeature {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
