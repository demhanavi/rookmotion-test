package com.example.rookmotiontest.screen.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.rookmotiontest.application.AppController;
import com.example.rookmotiontest.screen.login.core.LoginModel;
import com.example.rookmotiontest.screen.login.core.LoginPresenter;
import com.example.rookmotiontest.screen.login.core.LoginView;
import com.example.rookmotiontest.screen.login.dagger.DaggerLoginComponent;
import com.example.rookmotiontest.screen.login.dagger.LoginModule;
import com.example.rookmotiontest.screen.main.MainActivity;

import javax.inject.Inject;

public class LoginActivity extends AppCompatActivity {

    @Inject
    LoginModel model;
    @Inject
    LoginView view;
    @Inject
    LoginPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerLoginComponent.builder()
                .appComponent(AppController.getAppComponent())
                .loginModule(new LoginModule(this))
                .build()
                .inject(this);
        setContentView(presenter.getView());
        presenter.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    public void goToMainActivity() {
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
        finish();
    }
}
