package com.example.rookmotiontest.screen.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.example.rookmotiontest.application.AppController;
import com.example.rookmotiontest.screen.main.core.MainModel;
import com.example.rookmotiontest.screen.main.core.MainPresenter;
import com.example.rookmotiontest.screen.main.core.MainView;
import com.example.rookmotiontest.screen.main.dagger.DaggerMainComponent;
import com.example.rookmotiontest.screen.main.dagger.MainModule;
import com.example.rookmotiontest.util.imagePicker.ImagePicker;

import javax.inject.Inject;

import static com.example.rookmotiontest.screen.main.core.MainPresenter.PERMISSION_REQUEST_CODE;
import static com.example.rookmotiontest.screen.main.core.MainPresenter.PICK_IMAGE_ID;

public class MainActivity extends AppCompatActivity {

    @Inject
    MainPresenter presenter;
    @Inject
    MainView view;
    @Inject
    MainModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerMainComponent.builder()
                .appComponent(AppController.getAppComponent())
                .mainModule(new MainModule(this))
                .build()
                .inject(this);
        setContentView(presenter.getView());
        presenter.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if(permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    presenter.checkForCameraPermission();
                } else {
                    pickImage(PICK_IMAGE_ID);
                }
            } else {
                view.showToast("El permiso no fue dado.");
            }
        }
    }

    public void pickImage(int pickImageId) {
            Intent pickImageIntent = ImagePicker.getPickImageIntent(this, "selecciona tu imagen:");
            startActivityForResult(pickImageIntent, pickImageId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PICK_IMAGE_ID) {
            presenter.getImageFromIntent(ImagePicker.getImageFromResult(this,requestCode, resultCode, data));
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void sharePokemon(String header) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, header);
        startActivity(Intent.createChooser(sharingIntent, "Compartir"));
    }
}