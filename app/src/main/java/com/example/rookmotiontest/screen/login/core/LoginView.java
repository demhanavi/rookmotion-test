package com.example.rookmotiontest.screen.login.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rookmotiontest.R;
import com.example.rookmotiontest.screen.login.LoginActivity;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class LoginView {

    @BindView(R.id.user_edit_text)
    TextInputEditText userEditText;
    @BindView(R.id.password_edit_text)
    TextInputEditText passwordEditText;
    @BindView(R.id.login_button)
    Button loginButton;

    private View view;
    private Context context;
    private PublishSubject<Integer> clickSubjects = PublishSubject.create();

    public LoginView(LoginActivity context){
        this.context = context;
        initView();
    }

    private void initView(){
        FrameLayout parent = new FrameLayout(context);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(context).inflate(R.layout.activity_login, parent, true);
        ButterKnife.bind(this, view);
        loginButton.setOnClickListener(getOnClickListener());
        passwordEditText.setOnEditorActionListener(getEditorActionListener());
    }

    private View.OnClickListener getOnClickListener(){
        return v -> clickSubjects.onNext(v.getId());
    }

    private TextView.OnEditorActionListener getEditorActionListener(){
        return ((textView, actionId, keyEvent) -> {
           if(actionId == EditorInfo.IME_ACTION_DONE){
               hideKeyboard();
               return true;
           }
           return false;
        });
    }

    private void hideKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputMethodManager != null)
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    String getUser(){
        return userEditText.getText() == null ? "" : userEditText.getText().toString();
    }

    String getPassword(){
        return passwordEditText.getText() == null ? "" : passwordEditText.getText().toString();
    }

    void showToast(String toastMessage){
        Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();
    }

    View getView(){
        return view;
    }

    Observable<Integer> getClickSubjects() {
        return clickSubjects;
    }
}
