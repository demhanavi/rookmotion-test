package com.example.rookmotiontest.screen.login.dagger;

import com.example.rookmotiontest.application.builder.AppComponent;
import com.example.rookmotiontest.screen.login.LoginActivity;

import dagger.Component;

@LoginScope
@Component(modules = {LoginModule.class}, dependencies = {AppComponent.class})
public interface LoginComponent {
    void inject(LoginActivity context);
}
