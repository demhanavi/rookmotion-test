package com.example.rookmotiontest.screen.main.dagger;

import com.example.rookmotiontest.application.builder.AppComponent;
import com.example.rookmotiontest.screen.main.MainActivity;

import dagger.Component;

@MainScope
@Component(modules = {MainModule.class}, dependencies = {AppComponent.class})
public interface MainComponent {
    void inject(MainActivity context);
}
