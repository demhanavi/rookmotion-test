package com.example.rookmotiontest.screen.login.core;

import android.view.View;

import com.example.rookmotiontest.R;
import com.example.rookmotiontest.persistence.entities.User;
import com.example.rookmotiontest.screen.login.LoginActivity;
import com.example.rookmotiontest.util.rx.RxSchedulers;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class LoginPresenter {

    private RxSchedulers schedulers;
    private CompositeDisposable subscriptions = new CompositeDisposable();
    private LoginActivity context;
    private LoginView view;
    private LoginModel model;

    public LoginPresenter(LoginActivity context, LoginModel model, LoginView view, RxSchedulers schedulers){
        this.context = context;
        this.model = model;
        this.view = view;
        this.schedulers = schedulers;
    }

    public void onCreate(){
        subscriptions.add(handleClicks());
    }

    public void onDestroy(){
        subscriptions.clear();
    }

    private Disposable handleClicks(){
        return view.getClickSubjects().subscribe(viewId -> {
            if(viewId == R.id.login_button){
                doLogin();
            }
        });
    }
    private void doLogin(){
        if(!view.getUser().equals("") && !view.getPassword().equals("")){
            subscriptions.add(saveSessionData());
        } else {
            view.showToast("Campos vacíos, procure ingresar al menos un caracter en cada uno");
        }
    }

    private Disposable saveSessionData(){
        User user = new User();
        user.setUsername(view.getUser());
        user.setPassword(view.getPassword());
        return model.insertUser(user)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.androidThread())
                .subscribe(this::onSuccessSavedSession, this::onDatabaseError);
    }

    private void onSuccessSavedSession(){
        Timber.d("Save session completed!");
        context.goToMainActivity();
    }

    private void onDatabaseError(Throwable error){
        Timber.e("There was a database Error! %s", error.getMessage());
        view.showToast("Ocurrió un error al intentar iniciar sesión");
    }

    public View getView() {
        return view.getView();
    }
}
