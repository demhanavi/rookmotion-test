package com.example.rookmotiontest.screen.main.dagger;
import com.example.rookmotiontest.api.PokeApi;
import com.example.rookmotiontest.screen.main.MainActivity;
import com.example.rookmotiontest.screen.main.core.MainModel;
import com.example.rookmotiontest.screen.main.core.MainPresenter;
import com.example.rookmotiontest.screen.main.core.MainView;
import com.example.rookmotiontest.util.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    private MainActivity context;

    public MainModule(MainActivity context){
        this.context = context;
    }

    @MainScope
    @Provides
    MainView provideView(){
        return new MainView(context);
    }

    @MainScope
    @Provides
    MainPresenter providePresenter(MainModel model, MainView view, RxSchedulers schedulers){
        return new MainPresenter(context, model, view, schedulers);
    }

    @MainScope
    @Provides
    MainModel provideModel(PokeApi api){
        return new MainModel(api);
    }
}
