package com.example.rookmotiontest.screen.main.core;

import com.example.rookmotiontest.api.PokeApi;
import com.example.rookmotiontest.api.responses.PokemonResponse;

import io.reactivex.Observable;

public class MainModel {

    private PokeApi pokeApi;

    public MainModel(PokeApi pokeApi){
        this.pokeApi = pokeApi;
    }

    Observable<PokemonResponse> getRandomPokemon(long id){
        return pokeApi.getRandomPokemon(id);
    }

}
