package com.example.rookmotiontest.screen.main.core;

import android.Manifest;
import android.graphics.Bitmap;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.example.rookmotiontest.R;
import com.example.rookmotiontest.api.responses.PokemonResponse;
import com.example.rookmotiontest.screen.main.MainActivity;
import com.example.rookmotiontest.util.permissions.PermissionAskListener;
import com.example.rookmotiontest.util.permissions.PermissionUtil;
import com.example.rookmotiontest.util.rx.RxSchedulers;

import java.util.Random;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class MainPresenter implements PermissionAskListener {

    public static final int PICK_IMAGE_ID = 234;
    public static final int PERMISSION_REQUEST_CODE = 235;

    private MainActivity context;
    private MainView view;
    private MainModel model;
    private RxSchedulers schedulers;
    private CompositeDisposable subscriptions = new CompositeDisposable();
    private PermissionUtil permissionUtil;

    public MainPresenter(MainActivity context, MainModel model, MainView view, RxSchedulers schedulers){
        this.context = context;
        this.model = model;
        this.view = view;
        this.schedulers = schedulers;
        this.permissionUtil = new PermissionUtil(context);
    }

    public void onCreate(){
        subscriptions.add(handleClicks());
    }

    public void onDestroy(){
        subscriptions.clear();
    }

    private Disposable handleClicks(){
        return view.getClickSubjects().subscribe(viewId -> {
            if(viewId == R.id.load_image_button){
                checkForStoragePermission();
            } else {
                getAPokemon();
            }
        });
    }

    private void getAPokemon(){
        Disposable findAPokemon = model.getRandomPokemon(getRandomNumber())
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.androidThread())
        .subscribe(this::onPokemonFound, this::onPokemonError);
        subscriptions.add(findAPokemon);
    }

    private void onPokemonFound(PokemonResponse pokemonResponse) {
        view.showToast("Pokemon found! " + pokemonResponse.getName());
        sharePokemonInfo(pokemonResponse);
    }

    private void onPokemonError(Throwable error){
        view.showToast("Error finding pokemon!");
        Timber.e("Error finding pokemon! %s ", error.getMessage());
    }

    private void sharePokemonInfo(PokemonResponse pokemon){
        String header = "Hey! encontré sin querer un " + pokemon.getName() + ". ¿Quieres verlo?\n\n" + pokemon.getForms().getDefaultForm();
        context.sharePokemon(header);
    }

    private long getRandomNumber(){
        Random randomGenerator = new Random();
        return (long) randomGenerator.nextInt((150 - 1) + 1) + 1;
    }

    public void checkForCameraPermission() {
        permissionUtil.checkPermission(context, Manifest.permission.CAMERA, this, PERMISSION_REQUEST_CODE);
    }

    private void checkForStoragePermission(){
        permissionUtil.checkPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE, this, PERMISSION_REQUEST_CODE);
    }

    private void pickImage(){
        context.pickImage(PICK_IMAGE_ID);
    }

    public void getImageFromIntent(Bitmap image){
        view.setImage(image);
    }

    public View getView() {
        return view.getView();
    }

    @Override
    public void onNeedPermission(String permission, int requestCode) {
        ActivityCompat.requestPermissions(context, new String[]{permission}, requestCode);
    }

    @Override
    public void onPermissionPreviouslyDenied(String permission, int requestCode) {
        new AlertDialog.Builder(context)
                .setMessage("Algunos permisos son requeridos para ver las imágenes")
                .setPositiveButton("OK", (dialog, which) -> permissionUtil.checkPermission(context, permission, this, PERMISSION_REQUEST_CODE))
                .setNegativeButton("Cancelar", (dialog, which) -> {
                    dialog.dismiss();
                    view.showToast("El permiso no fue dado");
                });
    }

    @Override
    public void onPermissionDisabled() {
        view.showToast("El permiso no fue dado. Debe de activarlo desde configuración para abrir la cámara o la galería");
    }

    @Override
    public void onPermissionGranted(String permission) {
        if(permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            checkForCameraPermission();
        } else{
            pickImage();
        }
    }
}
