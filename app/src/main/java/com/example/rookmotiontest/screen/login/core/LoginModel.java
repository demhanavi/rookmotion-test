package com.example.rookmotiontest.screen.login.core;

import com.example.rookmotiontest.persistence.AppDatabase;
import com.example.rookmotiontest.persistence.entities.User;

import io.reactivex.Completable;

public class LoginModel {

    private AppDatabase appDatabase;

    public LoginModel(AppDatabase appDatabase){
        this.appDatabase = appDatabase;
    }

    Completable insertUser(User user){
        return Completable.fromAction(() -> appDatabase.userDao().insertUser(user));
    }
}
