package com.example.rookmotiontest.screen.login.dagger;

import com.example.rookmotiontest.persistence.AppDatabase;
import com.example.rookmotiontest.screen.login.LoginActivity;
import com.example.rookmotiontest.screen.login.core.LoginModel;
import com.example.rookmotiontest.screen.login.core.LoginPresenter;
import com.example.rookmotiontest.screen.login.core.LoginView;
import com.example.rookmotiontest.util.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    private LoginActivity context;

    public LoginModule(LoginActivity context){
        this.context = context;
    }

    @LoginScope
    @Provides
    LoginView provideView(){
        return new LoginView(context);
    }

    @LoginScope
    @Provides
    LoginPresenter providePresenter(LoginModel model, LoginView view, RxSchedulers schedulers){
        return new LoginPresenter(context, model, view, schedulers);
    }

    @LoginScope
    @Provides
    LoginModel provideModel(AppDatabase appDatabase){
        return new LoginModel(appDatabase);
    }
}
