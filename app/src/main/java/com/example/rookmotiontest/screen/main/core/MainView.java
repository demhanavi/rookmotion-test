package com.example.rookmotiontest.screen.main.core;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.rookmotiontest.R;
import com.example.rookmotiontest.screen.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class MainView {

    @BindView(R.id.display_image_view)
    ImageView displayImageView;
    @BindView(R.id.load_image_button)
    Button loadImageButton;
    @BindView(R.id.get_pokemon_button)
    Button getPokemonButton;

    private View view;
    private MainActivity context;
    private PublishSubject<Integer> clickSubjects = PublishSubject.create();

    public MainView(MainActivity context){
        this.context = context;
        initView();
    }

    private void initView(){
        FrameLayout parent = new FrameLayout(context);
        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view = LayoutInflater.from(context).inflate(R.layout.activity_main, parent, true);
        ButterKnife.bind(this, view);
        loadImageButton.setOnClickListener(getOnClickListener());
        getPokemonButton.setOnClickListener(getOnClickListener());
    }

    private View.OnClickListener getOnClickListener(){
        return v-> clickSubjects.onNext(v.getId());
    }

    public void setImage(Bitmap image) {
        displayImageView.setVisibility(View.VISIBLE);
        displayImageView.setImageBitmap(image);
    }

    public void showToast(String toastMessage){
        Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();
    }

    Observable<Integer> getClickSubjects() {
        return clickSubjects;
    }

    View getView() {
        return view;
    }
}
