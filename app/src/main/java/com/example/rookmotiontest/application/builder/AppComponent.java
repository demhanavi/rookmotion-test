package com.example.rookmotiontest.application.builder;

import com.example.rookmotiontest.api.PokeApi;
import com.example.rookmotiontest.application.builder.modules.AppContextModule;
import com.example.rookmotiontest.application.builder.modules.NetworkModule;
import com.example.rookmotiontest.application.builder.modules.PokeApiServiceModule;
import com.example.rookmotiontest.application.builder.modules.RxModule;
import com.example.rookmotiontest.persistence.AppDatabase;
import com.example.rookmotiontest.util.PropertiesReader;
import com.example.rookmotiontest.util.rx.RxSchedulers;

import dagger.Component;

@AppScope
@Component(modules = {AppContextModule.class, RxModule.class, NetworkModule.class, PokeApiServiceModule.class})
public interface AppComponent {
    RxSchedulers rxSchedulers();
    PropertiesReader propertiesReader();
    AppDatabase appDatabase();
    PokeApi pokeApi();
}
