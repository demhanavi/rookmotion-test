package com.example.rookmotiontest.application.builder.modules;

import android.content.Context;

import com.example.rookmotiontest.application.builder.AppScope;
import com.example.rookmotiontest.persistence.AppDatabase;
import com.example.rookmotiontest.util.PropertiesReader;

import dagger.Module;
import dagger.Provides;

@Module
public class AppContextModule {

    private final Context context;

    public AppContextModule(Context context){
        this.context = context;
    }

    @AppScope
    @Provides
    Context provideAppContext(){
        return context;
    }

    @AppScope
    @Provides
    PropertiesReader providePropertiesReader(){
        return new PropertiesReader(context);
    }

    @AppScope
    @Provides
    AppDatabase provideAppDatabase(){
        return AppDatabase.getInstance(context);
    }
}
