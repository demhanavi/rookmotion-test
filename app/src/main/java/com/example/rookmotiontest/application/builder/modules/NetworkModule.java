package com.example.rookmotiontest.application.builder.modules;

import android.content.Context;

import com.example.rookmotiontest.application.builder.AppScope;
import com.example.rookmotiontest.util.PropertiesReader;
import com.example.rookmotiontest.util.rx.AppRxSchedulers;

import java.io.File;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.BuildConfig;

@Module
public class NetworkModule {

    @AppScope
    @Provides
    OkHttpClient provideClient(PropertiesReader propertiesReader, HttpLoggingInterceptor logger, Cache cache){
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(propertiesReader.getConnectionTimeout(), TimeUnit.MILLISECONDS);
        client.writeTimeout(propertiesReader.getConnectionTimeout(), TimeUnit.MILLISECONDS);
        client.readTimeout(propertiesReader.getConnectionTimeout(), TimeUnit.MILLISECONDS);
        client.addInterceptor(logger);
        client.cache(cache);
        return client.build();
    }

    @AppScope
    @Provides
    HttpLoggingInterceptor provideLogger(){
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.level(HttpLoggingInterceptor.Level.BODY);
        return logger;
    }

    @AppScope
    @Provides
    Cache provideCache(File file){
        return new Cache(file, (10 * 10 * 1000));
    }

    @AppScope
    @Provides
    File provideCacheFile(Context context){
        return context.getFilesDir();
    }

    @AppScope
    @Provides
    RxJava2CallAdapterFactory provideRxAdapter(){
        return RxJava2CallAdapterFactory.createWithScheduler(AppRxSchedulers.INTERNET_SCHEDULERS);
    }

    @AppScope
    @Provides
    GsonConverterFactory provideConverterFactory(){
        return GsonConverterFactory.create();
    }
}
