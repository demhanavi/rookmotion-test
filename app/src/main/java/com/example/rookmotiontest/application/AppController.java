package com.example.rookmotiontest.application;

import android.app.Application;

import com.example.rookmotiontest.application.builder.AppComponent;
import com.example.rookmotiontest.application.builder.DaggerAppComponent;
import com.example.rookmotiontest.application.builder.modules.AppContextModule;
import com.example.rookmotiontest.util.PropertiesReader;

import timber.log.Timber;

public class AppController extends Application {

    private static AppComponent appComponent;
    PropertiesReader propertiesReader;

    @Override
    public void onCreate() {
        super.onCreate();
        initPropertiesReader();
        initializeLogger();
        initAppComponent();
    }

    private void initPropertiesReader(){
        propertiesReader = new PropertiesReader(getBaseContext());
    }

    private void initializeLogger(){
        Timber.plant(new Timber.DebugTree());
    }

    private void initAppComponent(){
        appComponent = DaggerAppComponent.builder().appContextModule(new AppContextModule(this)).build();
    }

    public static AppComponent getAppComponent(){
        return appComponent;
    }
}
