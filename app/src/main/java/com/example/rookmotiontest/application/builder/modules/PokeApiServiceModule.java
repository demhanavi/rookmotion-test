package com.example.rookmotiontest.application.builder.modules;

import com.example.rookmotiontest.api.PokeApi;
import com.example.rookmotiontest.application.builder.AppScope;
import com.example.rookmotiontest.util.PropertiesReader;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class PokeApiServiceModule {

    @AppScope
    @Provides
    PokeApi providePokeApi(PropertiesReader propertiesReader, OkHttpClient client, GsonConverterFactory gson, RxJava2CallAdapterFactory rxAdapter){
        Retrofit retrofitClient = new Retrofit.Builder()
                .client(client)
                .baseUrl(propertiesReader.getPokeBaseUrl())
                .addConverterFactory(gson)
                .addCallAdapterFactory(rxAdapter)
                .build();
        return retrofitClient.create(PokeApi.class);
    }
}
