package com.example.rookmotiontest.application.builder.modules;

import com.example.rookmotiontest.application.builder.AppScope;
import com.example.rookmotiontest.util.rx.AppRxSchedulers;
import com.example.rookmotiontest.util.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {

    @AppScope
    @Provides
    RxSchedulers provideRxSchedulers(){
        return new AppRxSchedulers();
    }
}
