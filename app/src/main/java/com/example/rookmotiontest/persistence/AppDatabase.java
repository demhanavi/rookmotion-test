package com.example.rookmotiontest.persistence;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.rookmotiontest.BuildConfig;
import com.example.rookmotiontest.persistence.daos.UserDao;
import com.example.rookmotiontest.persistence.entities.User;

@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class AppDatabase  extends RoomDatabase {

    private static final String DATABASE_NAME = BuildConfig.APPLICATION_ID + "_db";
    private static AppDatabase instance;

    public abstract UserDao userDao();

    public static synchronized AppDatabase getInstance(final Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
