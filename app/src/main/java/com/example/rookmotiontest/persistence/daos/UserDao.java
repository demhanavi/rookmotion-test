package com.example.rookmotiontest.persistence.daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.rookmotiontest.persistence.entities.User;

import java.util.List;

@Dao
public interface UserDao {

    @Transaction
    @Query("SELECT * FROM user")
    List<User> getUser();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);

    @Query("DELETE FROM user")
    void deleteUser();
}
