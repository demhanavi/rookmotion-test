package com.example.rookmotiontest.util;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import timber.log.BuildConfig;
import timber.log.Timber;

public class PropertiesReader {

    private static final String PROPERTIES_FILE_NAME = "application.properties";

    private Context context;
    private Properties properties;
    private InputStream propertiesStream;

    public PropertiesReader(Context context){
        this.context = context;
        initProperties();
    }

    private void initProperties(){
        properties = new Properties();
        try{
            propertiesStream = context.getAssets().open(PROPERTIES_FILE_NAME);
            properties.load(propertiesStream);
        } catch ( IOException exception){
            Timber.e("There was an initProperties Exception! %s", exception.getMessage());
        }
    }

    private String getBuildType(){
        return BuildConfig.BUILD_TYPE;
    }

    private Object getProperty(String propertyName){
        return properties.getProperty(propertyName);
    }

    public long getConnectionTimeout(){
        return Long.parseLong(getProperty("app.services.timeout").toString());
    }

    public String getPokeBaseUrl() {
        return getProperty("app.url.base").toString();
    }
}
