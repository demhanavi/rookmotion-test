package com.example.rookmotiontest.util.permissions;

public interface PermissionAskListener {

    void onNeedPermission(String permission, int requestCode);
    void onPermissionPreviouslyDenied(String permission, int requestCode);
    void onPermissionDisabled();
    void onPermissionGranted(String permission);
}
