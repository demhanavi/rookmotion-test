package com.example.rookmotiontest.util.permissions;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.rookmotiontest.util.PreferencesManager;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class PermissionUtil {

    public static final int REQUEST_CODE = 15;

    private PreferencesManager preferencesManager;
    private Context context;

    public PermissionUtil(Context context){
        this.context = context;
        preferencesManager = new PreferencesManager(context);
    }

    private boolean hasPermission(String permission){
        return ContextCompat.checkSelfPermission(context.getApplicationContext(), permission) != PERMISSION_GRANTED;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean askForMultiplePermissions(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            List<String> permissionsList = new ArrayList<>();
            if(!permissionsList.isEmpty()){
                String[] permissions = permissionsList.toArray(new String[permissionsList.size()]);
                ActivityCompat.requestPermissions((Activity) context, permissions, REQUEST_CODE);
                return true;
            } else {
                Timber.d("Not asking for permissions");
                return false;
            }
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void askForPermissions(String permission, int requestCode){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            String[] permissions = new String[]{permission};
            ActivityCompat.requestPermissions((Activity) context, permissions, requestCode);
        }
    }

    private boolean shouldAskPermission(Context context, String permission){
        if(shouldAskPermission()){
            int permissionResult = ContextCompat.checkSelfPermission(context, permission);
            return permissionResult != PERMISSION_GRANTED;
        }
        return false;
    }

    private boolean shouldAskPermission(){
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
    }

    public void checkPermission(Context context, String permission, PermissionAskListener listener, int requestCode){
        if(shouldAskPermission(context, permission)){
            if(ActivityCompat.shouldShowRequestPermissionRationale((AppCompatActivity) context, permission)){
                listener.onPermissionPreviouslyDenied(permission, requestCode);
            } else {
                if(preferencesManager.isFirstTimeAsking(permission)){
                    preferencesManager.firstTimeAsking(permission, false);
                    listener.onNeedPermission(permission, requestCode);
                } else{
                    listener.onPermissionDisabled();
                }
            }
        } else {
            listener.onPermissionGranted(permission);
        }
    }
}
