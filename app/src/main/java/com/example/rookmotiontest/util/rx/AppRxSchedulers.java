package com.example.rookmotiontest.util.rx;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AppRxSchedulers implements RxSchedulers {

    public static final Executor BACKGROUND_EXECUTOR = Executors.newCachedThreadPool();
    public static final Scheduler BACKGROUND_SCHEDULERS = Schedulers.from(BACKGROUND_EXECUTOR);
    public static final Executor INTERNET_EXECUTOR = Executors.newCachedThreadPool();
    public static final Scheduler INTERNET_SCHEDULERS = Schedulers.from(INTERNET_EXECUTOR);

    @Override
    public Scheduler runOnBackground() {
        return BACKGROUND_SCHEDULERS;
    }

    @Override
    public Scheduler io() {
        return Schedulers.io();
    }

    @Override
    public Scheduler compute() {
        return Schedulers.computation();
    }

    @Override
    public Scheduler androidThread() {
        return AndroidSchedulers.mainThread();
    }

    @Override
    public Scheduler internet() {
        return INTERNET_SCHEDULERS;
    }
}
