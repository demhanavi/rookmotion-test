package com.example.rookmotiontest.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private final String PREFERENCES_NAME = "MySharedPreferences";

    public PreferencesManager(Context context){
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void firstTimeAsking(String permission, boolean isFirstTime){
        if(editor == null){
            editor = sharedPreferences.edit();
        }
        editor.putBoolean(permission, isFirstTime).apply();
    }

    public boolean isFirstTimeAsking(String permission){
        return sharedPreferences.getBoolean(permission, true);
    }
}
